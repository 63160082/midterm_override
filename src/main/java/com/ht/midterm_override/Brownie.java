/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ht.midterm_override;

/**
 *
 * @author ACER
 */
public class Brownie extends Dessert {

    public Brownie(String name, int price, int amount) {
        super(name, price, amount);
        System.out.println("**************************************************");        
    }
    
    @Override
    public int cal(int price , int amount) { //คำนวณราคาที่ต้องชำระเงิน
        if(price <= 0) {  
            System.out.println("Can't Bu Brownie");
        }
        return price * amount;
    }
    
    @Override
    public void print() {
        System.out.println("You buy " + name + " amount : " + amount + " piece.  price : " + price + " bath.");
        System.out.println(" payment amount : " + cal(price , amount) + " bath.");     //ราคาทั้งหมดที่ต้องชำระ
    }    
    
}
