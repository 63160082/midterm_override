/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ht.midterm_override;

/**
 *
 * @author ACER
 */
public class TestDessert {
    public static void main(String[] args) {
        
        Daifuku dfk1 = new Daifuku("Daifuku",35,2);
        dfk1.print();
        Daifuku dfk2 = new Daifuku("Daifuku",0,2);
        dfk2.print();        
        
        Brownie bn1 = new Brownie("Brownie",10,12);
        bn1.print();
        Brownie bn2 = new Brownie("Brownie",0,12);
        bn2.print();        
        
        Macaron mcr1 = new Macaron("Macaron",35,9);
        mcr1.print();
        Macaron mcr2 = new Macaron("Macaron",0,9);
        mcr2.print();
        
    }
}
