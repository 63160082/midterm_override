/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ht.midterm_override;

/**
 *
 * @author ACER
 */
public class Dessert {
    protected String name;  //ชื่อขนม
    protected int price; //ราคาขนม หน่วยเป็น บาท
    protected int amount; //จำนวนขนมที่ซื้อ หน่วยเป็น ชิ้น

    public Dessert(String name, int price, int amount) {
        this.name = name;
        this.price = price;
        this.amount = amount;
    }
    
    public int cal(int price , int amount) { //คำนวณราคาที่ต้องชำระเงิน
        if(price <= 0) {  
            System.out.println(" ");
        }
        return price * amount;
    }
    
    public void print() {
        System.out.println("You buy " + name + " amount : " + amount + " piece.  price : " + price + " bath.");
        System.out.println(" payment amount : " + cal(price , amount) + " bath.");     //ราคาทั้งหมดที่ต้องชำระ
    }
    
}
